from flask import Flask, render_template, request, redirect, url_for
import requests
import json

# Flaskインスタンスを作成
# ＜ここに処理を追加＞

# APIのURLを定義
API_URL = 'http://localhost:8000/todo/'
API_URL_ALL = 'http://localhost:8000/all/'


@app.route('/')
def index():
    """
    Task一覧表のトップページを表示する
    """
    # 更新するタスクのedit対象のIDを取得する（なければ空）
    edit_id = request.args.get("edit")

    # ToDoリストを一括取得
    # ＜list_todo()関数から受け取った戻り値をjsonライブラリでdict形式に変換してtodo_json変数に格納する処理＞

    # index.htmlを表示
    return render_template(
        # ＜呼び出したいhtmlファイル(index.html)を指定＞
        todolist=todo_json["todolist"],
        edit=edit_id
    )


def list_todo() -> dict:
    """
    ToDoタスクの全取得
    """
    # APIへリクエスト
    res = requests.get(API_URL_ALL)
    return res.text


@app.route('/todo', methods=['POST'])
def create_todo():
    """
    ToDoタスクの追加

    title: タスクのタイトル
    done: タスクの完了状況(0: 未完了 / 1: 完了)
    deadline: タスクの対応期日(yyyy-mm-dd形式)
    """
    item = {
        "title": request.form.get('title'),
        "done": request.form.get('done'),
        "deadline": request.form.get('deadline')
    }

    # APIへリクエスト
    # ＜APIへのPOSTリクエスト処理を追加(requestsライブラリを使用)＞

    # トップページへredirect
    return redirect(url_for('index'))


@app.route('/updatetodo/<id>', methods=['POST'])
def update_todo(id: str):
    """
    ToDoタスクの更新

    id: タスクのID
    title: タスクのタイトル
    done: タスクの完了状況(0: 未完了 / 1: 完了)
    deadline: タスクの対応期日(yyyy-mm-dd形式)
    """
    params = {
        "id": id
    }
    item = {
        "title": request.form.get('title'),
        "done": request.form.get('done'),
        "deadline": request.form.get('deadline')
    }

    # APIへリクエスト
    # ＜APIへのPUTリクエスト処理を追加(requestsライブラリを使用)＞

    # トップページへredirect
    return redirect(url_for('index'))


@app.route('/deletetodo/<id>', methods=['GET'])
def delete_todo(id: str):
    """
    ToDoタスクの削除

    id: タスクのID
    """
    # ＜update_todo()関数と同様にparams変数にdict形式でidを設定する＞

    # APIへリクエスト
    # ＜APIへのDELETEリクエスト処理を追加(requestsライブラリを使用)＞

    # トップページへredirect
    return redirect(url_for('index'))


# Pythonスクリプトと起動された時に実行する
if __name__ == '__main__':
    # FlaskでWebサーバを8001ポートで起動
    # ＜ここに処理を追加＞
